ip2region
===============

ip地址转换区域处理  
使用 [Ip2region](https://github.com/lionsoul2014/ip2region) 进行处理

> 运行环境要求PHP7.1+。

## 安装
~~~
composer i
~~~

## 配置数据库及表结构
./config/schema.sql

## 运行
~~~
php think run
~~~

## 调用方式
~~~
~/ip/49.72.199.228
~~~

## 返回结果
~~~
{"code":0,"ip":"49.72.199.228","country":"中国","region":"江苏","city":"苏州","isp":"电信"}
~~~