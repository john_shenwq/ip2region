<?php

namespace app\controller;

use app\BaseController;
use ffhome\common\util\CommonUtil;
use Ip2Region;
use think\facade\Db;
use think\facade\Log;

class Index extends BaseController
{
    public function index()
    {
        return '<style type="text/css">*{ padding: 0; margin: 0; } div{ padding: 4px 48px;} a{color:#2E5CD5;cursor: pointer;text-decoration: none} body{ background: #fff; font-family: "Century Gothic","Microsoft yahei"; color: #333;font-size:18px;} p{ line-height: 1.6em; font-size: 42px }</style><div><p>IP地址查询处理</p></div>';
    }

    public function ip2Region($ip)
    {
        $ret = $this->getByIp2Region($ip);
        if ($ret['code'] == 0) {
            return json($ret);
        }
        $ret = $this->getByDb($ip);
        if ($ret['code'] == 0) {
            return json($ret);
        }
        Log::notice("从淘宝库中获取信息:${ip}");
        $ret = $this->getByTaobao($ip);
        if ($ret['code'] == 0) {
            $this->saveToDb($ret);
            return json($ret);
        }
        $ret = $this->getByIpApi($ip);
        if ($ret['code'] == 0) {
            $this->saveToDb($ret);
        }
        return json($ret);
    }

    private function getByIp2Region($ip)
    {
        try {
            $ip2region = new Ip2Region();
            $info = $ip2region->btreeSearch($ip);
            $region = explode('|', $info['region']);
            return [
                'code' => 0,
                'ip' => $ip,
                'country' => $region[0],
                'region' => $region[2],
                'city' => $region[3],
                'isp' => $region[4],
            ];
        } catch (\Exception $e) {
            return ['code' => -1];
        }
    }

    private function getByDb($ip)
    {
        $ret = Db::table('ip')->where('ip', $ip)->findOrEmpty();
        if (empty($ret)) {
            return ['code' => -1];
        }
        $ret['code'] = 0;
        return $ret;
    }

    private function getByTaobao($ip)
    {
        try {
            $ret = CommonUtil::http('https://ip.taobao.com/outGetIpInfo', ['ip' => $ip, 'accessKey' => 'alibaba-inc']);
            if ($ret['code'] == 0) {
                $ret = $ret['data'];
                return [
                    'code' => 0,
                    'ip' => $ip,
                    'country' => $ret['country'],
                    'region' => $ret['region'],
                    'city' => $ret['city'],
                    'isp' => $ret['isp'],
                ];
            }
        } catch (\Exception $e) {
        }
        return ['code' => -1];
    }

    private function getByIpApi($ip)
    {
        try {
            $ret = CommonUtil::http('http://ip-api.com/json/' . $ip, ['lang' => 'zh-CN']);
            if ($ret['status'] == 'success') {
                return [
                    'code' => 0,
                    'ip' => $ip,
                    'country' => $ret['country'],
                    'region' => $ret['regionName'],
                    'city' => $ret['city'],
                    'isp' => $ret['isp'],
                ];
            }
        } catch (\Exception $e) {
        }
        return ['code' => -1];
    }

    private function saveToDb($data)
    {
        unset($data['code']);
        Db::name('ip')->insert($data);
    }
}
