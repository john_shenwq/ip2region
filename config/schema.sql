drop table if exists ip;

create table ip
(
   ip                   varchar(15) not null default '' comment 'IP地址',
   country              varchar(50) not null default '' comment '国家',
   region               varchar(50) not null default '' comment '省份',
   city                 varchar(50) not null default '' comment '城市',
   isp                  varchar(50) not null default '' comment '运营商',
   primary key (ip)
);
alter table ip comment 'IP';
